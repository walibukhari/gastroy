<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>Laravel</title>

        <!-- Fonts -->
{{--        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">--}}

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
{{--        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">--}}
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <style>
            body {
                font-family: 'Josefin Sans', sans-serif;
                background: #EEF2F3;
            }
            header{
                background: #FFFFFF;
                height: 70px;
                position: fixed;
                z-index: 90;
                left: 0;
                top: 0;
                width: 100%;
                box-shadow: 0 2px 2px 0 rgba(0,0,0,.15);
            }
            .customFlex{
                display: flex;
                align-items: center;
                justify-content: space-between;
            }
            .customContainer{
                height: 70px;
                padding: 0px;
            }
            .t-bold{
                font-weight: 700;
            }
            .min-meny{
                color: #fa85a5;
                letter-spacing: .88px;
                font-size: 21px;
                font-weight: 700;
            }
            .custom-long{
                color: #e0e0e0;
            }
            .min-meny2{
                color:#dcdddc;
                letter-spacing: .88px;
                font-size: 21px;
            }
            .customA{
                display: flex;
                flex-direction: column;
                align-items: center;
            }
            .menu-area{
                display: flex;
                justify-content: space-between;
                margin-right: 20px;
                flex: 0.6;
                padding-bottom: 3px;
            }
            .customHeadText{
                color: #333;
                display: block;
                font-size: 18px;
                letter-spacing: .15em;
                padding-top: 5px;
                margin-bottom: 0px;
                margin: 0px;
                width: 220px;
                position: relative;
                justify-content: flex-start;
                flex-shrink: 1;
            }
            .mid{
                flex-shrink: 0;
                flex-grow: 1;
                text-align: center;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .customA > img {
                width: 30px;
                height: 36px;
                object-fit: contain;
            }
            .customA > span{
                font-size: 10px;
                letter-spacing: -.04px;
            }
            .customA{
                /*padding-left: 20px;*/
                /*padding-right: 20px;*/
            }
            .customcontainerPage{
                position: relative;
                top: 75px;
                padding: 0px;
                text-align: center;
            }
            .text1{
                padding-top: 10px;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 16px;
                color: #202020;
                margin-bottom: 3px;
            }
            .text2{
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.67;
                color: #212529;
            }
            .mid-page-content{
                margin-top: 20px;
                margin-bottom: 30px;
            }
            .btnbtn{
                margin-top: 25px;
                border-radius: 25px;
                border: 1px solid transparent;
                font-size: 12px;
                height: 42px;
                cursor: pointer;
                text-align: center;
                outline: none!important;
                text-decoration: none!important;
                width: 249px;
                font-weight: 700;
            }
            .section-1{
                display: flex;
                flex-direction: column;
                align-items: center;
            }
            .section-2 > p{
                font-size: 12px;
                padding-top: 15px;
                padding-bottom: 15px;
                font-style: italic;
                color: #333;    margin-left: 60px;
                margin-right: 60px;
                padding-bottom: 60px;
            }
            .section-2{
                justify-content: center;
                display: flex;
                flex-direction: column;
                align-items: center;
            }
            .section-3{
                display: flex;
                flex-direction: column;
                align-items: center;
            }
            .between-page-content{
                margin-left: auto;
                margin-right: auto;
                width: 100%;
                padding-left: 25px;
                padding-right: 25px;
                padding-top: 20px;
                max-width: 400px;
                margin-bottom: 8rem;
                display: flex;
                justify-content: center;
            }
            .btn-pink {
                color: #fff;
                background-color: #fa85a5;
            }
            .btn-shadow {
                box-shadow: 0 2px 8px 0 rgba(0,0,0,.1);
            }
            @media (max-width: 767px) {
                .between-page-content{
                    display: block !important;
                }
                .customContainer {
                    height: 70px;
                    padding: 0px;
                    width: 100%;
                    flex: 100%;
                    max-width: 94%;
                }
            }
            @media (max-width: 597px) {
                .menu-area{
                    display: none;
                }
            }
            @media (max-width: 414px) {
                .menu-area{
                    display: none;
                }
                .mid{
                    display: none;
                }
            }
            .btn-yellow {
                background-color: #fff57f;
                border-radius: 25px;
            }
            .customP{
                text-align: center;
                text-transform: uppercase;
                font-weight: 700;
                font-size: 1.3rem;
                padding-bottom: 1rem;
                margin-bottom: .5rem;
            }
            .customP2{
                text-align: center;
                padding-bottom: 0.93rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.67;
                color: #212529;
            }
            .btn-c{
                padding-top: 12px;
                padding-bottom: 10px;
                color: #333;
                font-weight: 700;
                width: 215px;
                display: block;
                margin-left: auto;
                margin-right: auto;
                font-size: 12px;
            }
            .modal-footer{
                border-top:0px !important;
            }
            .modal-content {
                position: relative;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                pointer-events: auto;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid rgba(0,0,0,.2);
                border-radius: .3rem;
                outline: 0;
                border-radius: 14px;
                height: 650px;
                width: 100%;
            }
        </style>
        <script type="text/javascript" async="" src="https://widget.intercom.io/widget/dcrozvjc"></script>
    </head>
    <body class="antialiased">
        <header class="customFlex">
            <div class="container customContainer customFlex">
                <h3 class="customHeadText t-bold">GASTROFY</h3>
                <div class="mid">
                    <span class="min-meny">Min meny</span>
                    <img src="{{asset('arrow.png')}}" style="" />
                    <span class="min-meny2">Mina varor</span>
                </div>
                <div class="menu-area">
                    <a href="" class="customA">
                        <img src="{{asset('green.png')}}" />
                        <span style="color: #57ca8e;">Min matkasse</span>
                    </a>
                    <a href="" class="customA">
                        <img src="{{asset('ssss.png')}}" />
                        <span>Laga</span>
                    </a>
                    <a href="" class="customA">
                        <img src="{{asset('users.png')}}" style="object-fit: contain;" />
                        <span>Logga in</span>
                    </a>
                </div>
            </div>
        </header>
        <div class="container customcontainerPage">
            <div class="mid-page-content">
                <img src="{{asset('forks.png')}}" />
                <h3 class="text1">PLANERA MÅLTIDER</h3>
                <p class="text2">Välj vad ni ska äta framöver:</p>
            </div>
            <div class="between-page-content">
                <div class="section-1">
                    <img src="{{asset('ghost-menu.jpg')}}" />
                    <button data-toggle="modal" data-target="#myModal" class="button btnbtn btn-pink btn-shadow flex-cc">FÅ PERSONLIGT MENYFÖRSLAG</button>
                </div>
                <div class="section-2">
                    <p>eller</p>
                </div>
                <div class="section-3">
                    <img src="{{asset('ghost-recipe.jpg')}}" />
                    <button class="button btnbtn btn-pink btn-shadow flex-cc">FÅ PERSONLIGT MENYFÖRSLAG</button>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" style="text-align: left;
    padding-left: 12px;    margin-top: 12px;
    margin-bottom: 15px;" class="close" data-dismiss="modal"><img src="{{asset('closeImage.png')}}" /></button>
                    <div class="modal-body">
                        <p class="customP">FÅ ETT SKRÄDDARSYTT MENYFÖRSLAG</p>
                        <p class="customP2">Vi kommer nu ställa några korta frågor för att kunna skapa ett menyförslag med recept som vi hoppas passar dig. </p>
                    </div>
                    <div class="modal-footer" style="    display: flex;
    justify-content: center;">
                        <button type="button" class="btn-c btn-yellow next-btn" data-dismiss="modal">SKAPA MENYFÖRSLAG</button>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
